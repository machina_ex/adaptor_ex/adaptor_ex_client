import { minimal } from "minimal-schema";
import { nanoid } from "nanoid";
import { cloneDeep } from "lodash";

import text from "@/locale/en.json";

/*
 * returns new token or null
 */
export function setAuthToken(username, password) {
  let token = getCrossSessionItem("authtoken");
  if (username !== undefined && password !== undefined) {
    token = btoa(`${username}:${password}`);
    setCrossSessionItem("authtoken", token);
  }
  return token;
}

export function removeAuthToken() {
  localStorage.removeItem("authtoken");
}

export function setCrossSessionItem(key, value) {
  return localStorage.setItem(key, value);
}

export function getCrossSessionItem(key) {
  return localStorage.getItem(key);
}

export function removeCrossSessionItem(key) {
  return localStorage.removeItem(key);
}

/*
 * Because Adaptor uses different protocols and languages we restrict naming of
 * States, Actions, Games etc to these characters:
 * ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789
 * -._~:#&
 * no whitespaces
 */
export function isUrlSafe(str) {
  if (!str) return false;
  const regexp = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789\-._~:#']+$/;
  return regexp.test(str);
}

/*
 * Calculates difference between two Dates.
 * Returns String formatted as "DD days hh:mm:ss", below 24 hours difference
 * days are omitted.
 */
export function getTimeDiffString(datePast, dateNow) {
  const dayInSeconds = 86400,
    hourInSeconds = 3600,
    minuteInSeconds = 60;

  if (typeof datePast === "string") datePast = new Date(datePast);
  if (typeof dateNow === "string") dateNow = new Date(dateNow);
  let diffInSeconds = Math.abs(dateNow - datePast) / 1000;

  const days = Math.floor(diffInSeconds / dayInSeconds);
  diffInSeconds -= days * dayInSeconds;
  const hours = Math.floor(diffInSeconds / hourInSeconds);
  diffInSeconds -= hours * hourInSeconds;
  const minutes = Math.floor(diffInSeconds / minuteInSeconds);
  diffInSeconds -= minutes * minuteInSeconds;
  const seconds = Math.floor(diffInSeconds);

  let formatted = "";
  if (days > 0) formatted += days === 1 ? `${days} day ` : `${days} days `;
  formatted += hours < 10 ? `0${hours}:` : `${hours}:`;
  formatted += minutes < 10 ? `0${minutes}:` : `${minutes}:`;
  formatted += seconds < 10 ? `0${seconds}` : `${seconds}`;

  return formatted;
}

/*
 * Helper for i18n
 */
function getPropByStringKey(o, s) {
  s = s.replace(/\[(\w+)\]/g, ".$1"); // convert indexes to properties
  s = s.replace(/^\./, ""); // strip a leading dot
  const a = s.split(".");
  for (let i = 0, n = a.length; i < n; ++i) {
    const k = a[i];
    if (k in o) {
      o = o[k];
    } else {
      return;
    }
  }
  return o;
}

/*
 * Helper for i18n
 */
function renderTemplate(str, obj) {
  if (!obj) return str;
  for (const prop in obj) {
    str = str.replace(new RegExp("{" + prop + "}", "g"), obj[prop]);
  }
  return str;
}

// e.g.: i18n("ERROR.CHARACTERS", { variable_1: "game" })
export function i18n(key, vars) {
  const str = getPropByStringKey(text, key) || "";
  const result = renderTemplate(str, vars) || "";
  return result;
}

export function minimalObject(schema) {
  const obj = minimal(cloneDeep(schema));
  return obj;
}

// returns list of next objects e.g.: [ {next:"_state2_"} ]
export function findAllByKey(obj, keyToFind) {
  if (!obj) return [];
  else
    return Object.entries(obj).reduce(
      (acc, [key, value]) =>
        key === keyToFind
          ? acc.concat({ [keyToFind]: value })
          : typeof value === "object"
            ? acc.concat(findAllByKey(value, keyToFind))
            : acc,
      [],
    );
}

export function stringToColor(str) {
  let hash = 0;
  for (let i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  let colour = "#";
  for (let i = 0; i < 3; i++) {
    let value = (hash >> (i * 8)) & 0xff;
    colour += ("00" + value.toString(16)).substr(-2);
  }
  return colour;
}

/**
 * checks if object has key. use dot notation for nested keys
 *
 * @param {Object}  object  object to check key
 * @param {String}  key     name of the key or nested key like: 'key.key2'
 */
export function hasKey(object, key) {
  if (!object || typeof object !== "object") return false;
  if (!key || typeof key !== "string") return false;

  const keys = key.split(".");
  return hasNestedKeys(object, keys);
}

// helper for hasKey
function hasNestedKeys(object, keys) {
  for (let index = 0; index < keys.length; index++) {
    const key = keys[index];
    if (Object.prototype.hasOwnProperty.call(object, key)) {
      if (index < keys.length - 1) object = object[key];
      else return true;
    } else {
      return false;
    }
  }
}

/**
 * checks if object has key. use dot notation for nested keys and if the key is not an empty object
 *
 * @param {Object}  obj           object to check key
 * @param {String}  nestedKey     name of the key or nested key like: 'key.key2'
 */
export function hasKeyPresentAndPopulated(obj, nestedKey) {
  if (!obj || typeof obj !== "object") return false;
  if (!nestedKey || typeof nestedKey !== "string") return false;

  const keys = nestedKey.split(".");
  let currentObj = obj;

  for (const key of keys) {
    if (!Object.prototype.hasOwnProperty.call(currentObj, key)) {
      return false;
    }
    currentObj = currentObj[key];
  }

  return typeof currentObj === "object" && Object.keys(currentObj).length !== 0;
}

export function createId(size) {
  return nanoid(size);
}

/**
 * Escape regex characters
 * http://stackoverflow.com/a/6969486
 */
export function escapeRegExpChars(value) {
  if (!value) return value;
  // eslint-disable-next-line
  return value.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}
