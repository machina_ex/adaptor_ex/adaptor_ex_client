import { cloneDeep, clone, merge, isEqual } from "lodash";
import { isUrlSafe, i18n, findAllByKey, minimalObject, hasKey, createId, stringToColor } from "@utils";

const state = {
  selected: "", // name of selected level
  id: "",
  data: {},
  schema: {},
  variables: {},
  paths: [], // list of all paths combinations in level
  selectedState: "",
  selectedAction: "",
  copiedAction: undefined,
  busyStates: {}, // states that are occupied by other user
  editingStateName: null, // Add these new state properties
  addingStateComment: null,
};

const getters = {
  levelArgs: (state, getters, rootState) => (id) => {
    const level = rootState.game.levelList.find((l) => l._id === id) || {};
    return cloneDeep(level.config.arguments);
  },
  variablesForToolbar: (state) => (key) => {
    if (!state.variables[key]) return [];
    return state.variables[key].map((item) => {
      let newitem = {};
      let arr = item.split(".");
      let depth = arr.length - 1;
      let title = arr[depth].replace("]]", "").replace("[[", "");
      newitem["value"] = item;
      newitem["title"] = title;
      newitem["depth"] = depth;
      return newitem;
    });
  },
  statesNameList: (state) => {
    if (!state.data?.states) return [];
    return Object.values(state.data.states).reduce((accu, curr) => {
      accu.push(curr.name);
      return accu;
    }, []);
  },
  stateByName: (state) => (name) => {
    const states = state.data.states;
    // eslint-disable-next-line no-unused-vars
    const s = Object.entries(states).find(([k, v]) => v.name == name);
    if (s) return Object.assign({}, s[1], { id: s[0] });
    else return undefined;
  },
  stateIdByName: (state) => (name) => {
    const states = state.data.states;
    // eslint-disable-next-line no-unused-vars
    const s = Object.entries(states).find(([k, v]) => v.name == name);
    if (s) return s[0];
    else return undefined;
  },
  actionInfo: (state, getters, rootState) => (actionId) => {
    const action = getters.actionByIdResolved(actionId);
    if (!action) return undefined;
    const definition = rootState.game.actions.find((a) => a.action == action.action);
    let info;
    try {
      // FIXME: this should be rendered by server not in client
      const renderFunc = new Function("payload", "action", definition?.template);
      info = renderFunc(action.payload, action);
    } catch (error) {
      console.error(error);
      console.log(action, definition?.template);
    }
    return {
      action: action.action,
      name: action.name,
      mode: definition?.mode,
      plugin: action.plugin,
      data: action.payload || {},
      schema: definition?.schema || {},
      title: info?.title || action.name || action.action,
      subtitle: info?.subtitle || "",
      body: info?.body || findAllByKey(action.payload || {}, "next"),
    };
  },
  actionByIdResolved: (state, getters) => (actionId) => {
    if (!state.data.actions?.[actionId]) return;
    const resolved = cloneDeep(state.data.actions[actionId]);
    function resolveContent(obj) {
      for (let key in obj) {
        if (typeof obj[key] === "object") {
          resolveContent(obj[key]);
        } else if (typeof obj[key] === "string" && obj[key].startsWith("_content")) {
          obj[key] = getters.contentById(obj[key]);
        }
      }
      return obj;
    }
    resolveContent(resolved);
    return resolved;
  },
  contentById: (state) => (contentId) => {
    // TODO: multi language support
    const content = state.data.contents?.[contentId]?.["de"];
    if (content == undefined)
      console.error(`No Content 'de' for ${contentId} in: ${state.level?.contents?.[contentId]}`);
    return content ?? ""; // nullish coalescing to returns its right-hand side operand when its left-hand side operand is null or undefined
  },
  contentIdsByAction: (state) => (actionId) => {
    let contents = [];
    function resolveContent(obj) {
      for (let key in obj) {
        if (typeof obj[key] === "object") {
          resolveContent(obj[key]);
        } else if (typeof obj[key] === "string" && obj[key].startsWith("_content")) {
          contents.push(obj[key]);
        }
      }
      return obj;
    }
    resolveContent(state.data.actions[actionId]);
    return contents;
  },
  actionsByState: (state) => (stateId) => {
    const stateData = state.data.states[stateId];
    return [...stateData.listen, ...stateData.run].reduce(
      (res, key) => ((res[key] = state.data.actions[key]), res),
      {},
    );
  },
  contentsByState: (state, getters) => (stateId) => {
    const stateActions = getters.actionsByState(stateId);
    const allContentIds = Object.keys(state.data.contents);
    let values = [];
    const getAllValues = (obj) => {
      for (let key in obj) {
        typeof obj[key] === "object" ? getAllValues(obj[key]) : values.push(obj[key]);
      }
    };
    getAllValues(stateActions);
    const stateContentIds = values.filter((value) => allContentIds.includes(value));
    return stateContentIds.reduce((res, key) => ((res[key] = state.data.contents[key]), res), {});
  },
  actionsByActionTypeInState: (state, getters) => (stateId, type) => {
    const stateActions = getters.actionsByState(stateId);
    let actions = {};
    Object.keys(stateActions).forEach((key) => {
      if (stateActions[key]["action"] === type) actions[key] = stateActions[key];
    });
    return actions;
  },
  sumActionsInState: (state, getters) => (stateId, actionType) => {
    const all = Object.entries(getters.actionsByState(stateId));
    return actionType
      ? // eslint-disable-next-line no-unused-vars
        all.filter(([key, value]) => (value ? value.action === actionType : false)).length
      : Object.keys(all).length;
  },
  colorByStatePath: (state, getters) => (stateName) => {
    const stateId = getters.stateIdByName(stateName);
    if (hasKey(state.data.states, `${stateId}.path`)) {
      const path = state.data.states[stateId].path;
      const str = path.join();
      return !str || str === "start" ? "#000000" : stringToColor(str);
    } else {
      return "#000000";
    }
  },
  stateNameById: (state) => (stateId) => {
    return state.data.states[stateId]?.name || stateId;
  },
  actionNameById: (state) => (actionId) => {
    const action = state.data.actions[actionId];
    if (!action) return actionId;
    return action.name || action.action || actionId;
  },
};

const actions = {
  select({ dispatch }, { game, level }) {
    if (level !== state.selected && level !== state.id) dispatch("clearLevel");
    dispatch("getLevel", { game, level });
  },
  async getLevel({ commit, dispatch }, { game, level }) {
    const [data, actions, schema, variables, media] = await Promise.all([
      this.$api.getLevel({ game, level }).then((r) => r.data),
      this.$api.getActions({ game }).then((r) => r.data),
      this.$api.getLevelSchema({ game }).then((r) => r.data),
      this.$api.getLevelVariables({ game, level }).then((r) => r.data),
      this.$api.getMedia({ game }).then((r) => r.data),
    ]).catch((error) => {
      // TODO: reroute to level or game selection and error message
      console.error(error);
    });
    commit("SELECT_LEVEL_ID", data._id);
    commit("SELECT_LEVEL", data.name);
    commit("SET_LEVEL_DATA", { key: "data", value: data });
    dispatch("game/updateActionsList", actions, { root: true });
    commit("SET_LEVEL_DATA", { key: "schema", value: schema });
    commit("SET_LEVEL_DATA", { key: "variables", value: variables });
    dispatch("game/updateMediaList", media, { root: true });
    commit("SET_LEVEL_DATA", { key: "id", value: data._id });
  },
  clearLevel({ commit, dispatch }) {
    dispatch("adaptor/clearOtherUsers", undefined, { root: true });
    commit("CLEAR_LEVEL");
  },
  edit({ rootState, state, dispatch }, { operator, query }) {
    const game = rootState.game.selected;
    const level = state.id;
    this.$api
      .editLevel({ game, level, operator }, query)
      .then(() => dispatch("getLevel", { game, level }))
      .catch(console.error);
  },
  editName({ rootState, state, dispatch }, newLevelName) {
    const game = rootState.game.selected;
    const level = state.id;
    return this.$api
      .editLevel({ game, level, operator: "_set" }, { name: newLevelName })
      .then(() => {
        // if (rootState.adaptor.view == "Editor" || rootState.adaptor.view == "Config") {
        const levelParam = this.$router.currentRoute.value?.params?.level;
        if (!levelParam || levelParam != newLevelName) {
          this.$router.replace({ params: { game, level: newLevelName } });
        }
        // }
      })
      .catch((error) => {
        if (error.response.status == 400) {
          const message = i18n("ERROR.DUPLICATE", { target: "level", name: newLevelName });
          dispatch("adaptor/addLog", { type: "error", message }, { root: true });
        } else {
          const message = i18n("ERROR.GENERIC");
          dispatch("adaptor/addLog", { type: "error", message }, { root: true });
        }
        return Promise.reject(error);
      });
  },
  editStatus({ rootState, dispatch }, { level, status }) {
    const game = rootState.game.selected;
    this.$api
      .editLevel({ game, level, operator: "_set" }, { "config.status": status })
      .then(() => dispatch("game/getGame", game, { root: true }))
      .catch(console.error);
  },
  updateVariables({ commit }, variables) {
    commit("SET_LEVEL_DATA", { key: "variables", value: variables });
  },
  updatePaths({ commit }, data) {
    if (Object.prototype.hasOwnProperty.call(data, "paths")) {
      commit("SET_LEVEL_DATA", { key: "paths", value: data.path });
    }
    for (const stateId in state.data.states) {
      const path = Object.hasOwnProperty.call(data, stateId) ? data[stateId].path : [];
      commit("UPDATE_STATE_PATH", { stateId, path });
    }
  },
  updateStatePosition({ commit, dispatch }, { stateId, position }) {
    if (isEqual(position, state.data.states?.[stateId]?.view?.position)) return;
    commit("UPDATE_STATE_POSITION", { stateId, position });
    dispatch("syncState", stateId);

    // const game = rootState.game.selected;
    // const level = state.id;
    // const payload = { state: state.data.states[stateId] };
    // this.$api.updateState({ game, level, state: stateId }, payload);
  },
  updateStates({ commit }, data) {
    const { states, actions, contents } = data;
    Object.entries(actions).forEach(([actionId, data]) => {
      commit("SET_ACTION_DATA", { actionId, value: data });
    });
    Object.entries(states).forEach(([stateId, data]) => {
      commit("SET_LEVEL_STATE", { stateId, value: data });
    });
    Object.entries(contents).forEach(([contentId, value]) => {
      commit("SET_CONTENT", { contentId, value });
    });
  },
  updateStateName: ({ commit, getters, dispatch }, { stateId, name }) => {
    if (getters.statesNameList.includes(name)) {
      const message = i18n("ERROR.DUPLICATE", { target: "state", name });
      dispatch("adaptor/addLog", { type: "error", message }, { root: true });
      return Promise.reject(new Error("duplicate game name: " + name));
    } else if (!isUrlSafe(name) || name === undefined) {
      const message = i18n("ERROR.CHARACTERS", { target: "state", name });
      dispatch("adaptor/addLog", { type: "error", message }, { root: true });
      return Promise.reject(new Error("forbbiden characters in name: " + name));
    } else {
      commit("UPDATE_LEVEL_STATE", { stateId, value: { name } });
      dispatch("syncState", stateId);
    }
  },
  updateStateLocal({ commit }, { stateId, value }) {
    commit("UPDATE_LEVEL_STATE", { stateId, value });
  },
  deleteStateLocal({ commit }, stateId) {
    // TODO: delete contents and actions of state
    commit("DELETE_STATE", stateId);
  },
  async syncState({ rootState, state, dispatch, getters }, stateId) {
    const game = rootState.game.selected;
    const level = state.id;
    const payload = {
      states: { [stateId]: state.data.states[stateId] },
      actions: getters.actionsByState(stateId),
      contents: getters.contentsByState(stateId),
    };

    try {
      await this.$api.createState({ game, level }, payload);
    } catch (error) {
      console.error("Failed to sync state:", error);
      const message = i18n("ERROR.SYNC_STATE");
      dispatch("adaptor/addLog", { type: "error", message }, { root: true });
    }
  },
  blockState({ commit }, data) {
    commit("BLOCK_STATE", { stateId: data.state, session: data.user_session });
  },
  releaseState({ commit }, data) {
    commit("RELEASE_STATE", data);
  },
  selectState: ({ rootState, dispatch, commit }, stateId) => {
    if (rootState.live.isLive) return;
    if (stateId in state.busyStates) {
      const user = state.busyStates[stateId]?.user;
      const stateName = state.data.states[stateId]?.name;
      const message = i18n("STATE.BUSY", { stateName, user });
      dispatch("adaptor/addLog", { type: "info", message }, { root: true });
      return;
    }
    if (state.selectedAction) return;
    commit("SELECT_STATE", stateId);
  },
  unselectState({ commit }, stateId) {
    if (state.selectedState === stateId) {
      if (!state.selectedAction) {
        commit("SELECT_STATE", undefined);
        return true;
      }
    } else return false;
  },
  selectAction({ state, rootState, dispatch, commit }, { stateId, actionId }) {
    if (stateId in state.busyStates) {
      const user = state.busyStates[stateId]?.user;
      const stateName = state.data.states[stateId]?.name;
      const message = i18n("STATE.BUSY", { stateName, user });
      dispatch("adaptor/addLog", { type: "info", message }, { root: true });
      return;
    }
    const actionPlugin = state.data.actions[actionId].plugin;
    if (!rootState.plugins.pluginsNames.includes(actionPlugin)) {
      const message = i18n("ERROR.ACTION_PLUGIN_MISSING", { plugin: actionPlugin });
      dispatch("adaptor/addLog", { type: "error", message }, { root: true });
      return;
    }
    commit("SELECT_ACTION", state.selectedAction == actionId ? "" : actionId);
    commit("SELECT_STATE", stateId);
  },
  unselectAll({ commit }) {
    commit("SELECT_ACTION", undefined);
    commit("SELECT_STATE", undefined);
  },
  async addState({ commit, dispatch, getters }, { stateId, name, actionName, actionData, position }) {
    stateId = stateId || "_state_" + createId(8);

    if (name) {
      if (getters.statesNameList.includes(name)) {
        const message = i18n("ERROR.DUPLICATE", { target: "state", name });
        dispatch("adaptor/addLog", { type: "error", message }, { root: true });
        return Promise.reject(new Error("duplicate state name: " + name));
      } else if (!isUrlSafe(name)) {
        const message = i18n("ERROR.CHARACTERS", { target: "state", name });
        dispatch("adaptor/addLog", { type: "error", message }, { root: true });
        return Promise.reject(new Error("forbbiden characters in name: " + name));
      }
    }

    commit("ADD_STATE", { stateId, position });

    if (name) {
      commit("UPDATE_LEVEL_STATE", { stateId, value: { name } });
    }

    if (actionName) {
      await dispatch("addAction", { stateId, actionName, actionData });
    }

    // Sync the new state with the server
    await dispatch("syncState", stateId);

    return stateId;
  },
  createStateFromNext({ state, dispatch }, name) {
    const parentState = state.data.states[state.selectedState];
    const position = {
      x: parentState.view.position.x + 400,
      y: parentState.view.position.y + (parentState.view.position.y < 150 ? 100 : -100),
    };

    return dispatch("addState", {
      name,
      position,
      actionName: "next",
    });
  },
  moveExistingActionToNewState({ commit, dispatch }, { originState, actionId, position }) {
    const stateId = "_state_" + createId(8);
    commit("ADD_STATE", { stateId, position });
    dispatch("moveExistingAction", { stateId, originState, actionId });
  },
  moveExistingAction({ commit, dispatch, getters }, { stateId, originState, actionId }) {
    if (stateId == originState) return;

    const mode = state.data.actions[actionId].mode;

    const data = getters.actionByIdResolved(actionId);
    dispatch("addAction", {
      stateId,
      actionName: data.action,
      actionData: data.payload,
    })
      .then(() => {
        commit("DELETE_ACTION_KEY", { stateId: originState, actionId, mode });
        dispatch("syncState", originState);
      })
      .catch((error) => console.error(error));
  },
  shiftAction({ commit, dispatch }, { stateId, mode, index, direction }) {
    commit("SHIFT_ACTION", { stateId, mode, index, direction });
    dispatch("syncState", stateId);
  },
  duplicateState({ commit, getters, dispatch }, stateId) {
    const original = state.data.states[stateId];
    const newId = "_state_" + createId(8);
    const newName = `COPY_${original.name}_${createId(2)}`;
    const position = {
      x: original.view.position.x + 200,
      y: original.view.position.y + (original.view.position.y < 150 ? 100 : -100),
    };
    commit("ADD_STATE", { stateId: newId, stateName: newName, position });

    const actions = getters.actionsByState(stateId);
    for (const key in actions) {
      if (Object.hasOwnProperty.call(actions, key)) {
        const data = getters.actionByIdResolved(key);
        dispatch("addAction", {
          stateId: newId,
          actionName: data.action,
          actionData: data.payload,
        });
      }
    }
    // Sync the new state with the server
    dispatch("syncState", stateId);
  },
  async deleteState({ rootState, state, commit }, stateId) {
    const game = rootState.game.selected;
    const level = state.id;

    try {
      await this.$api.deleteState({ game, level, state: stateId });
    } catch (error) {
      // If we get a 404, the state hasn't been synced yet
      // Just delete it locally without throwing an error
      if (error.response?.status !== 404) {
        console.error(error);
        return;
      }
    }

    commit("DELETE_STATE", stateId);
  },
  copyActionById({ getters, dispatch }, actionId) {
    const action = getters.actionInfo(actionId);
    console.log("copyActionById", action);
    const data = {
      actionName: action.action,
      actionData: action.data,
      actionTitle: action.title,
    };
    dispatch("copyAction", data);
  },
  copyAction({ commit }, data) {
    commit("SET_ACTION_COPY", data);
  },
  pasteAction({ state, dispatch, commit }, stateId) {
    const { actionName, actionData } = state.copiedAction;
    // commit("RESET_ACTION_COPY");
    if ((actionName, actionData)) {
      dispatch("addAction", { stateId, actionName, actionData });
    }
  },
  addAction({ rootState, state, getters, dispatch, commit }, { stateId, actionName, actionData }) {
    console.log("add action", actionName);
    // TODO getter based on new actions list format
    const definition = rootState.game.actions.find((a) => a.action == actionName);
    const actionId = definition.action + "_" + createId(10);

    // Note: handle next action in state
    const nextsInState = Object.entries(getters.actionsByActionTypeInState(stateId, "next"));
    const listener = Object.entries(state.data.states[stateId]["listen"]);

    if (nextsInState.length > 0 && actionName == "next") {
      const message = i18n("ERROR.MAX_NEXT");
      dispatch("adaptor/addLog", { type: "error", message }, { root: true });
      return Promise.reject(message);
    }
    if (listener.length > 0 && actionName == "next") {
      const message = i18n("ERROR.NO_NEXT_BESIDE_LISTENER");
      dispatch("adaptor/addLog", { type: "error", message }, { root: true });
      return Promise.reject(message);
    }

    if (nextsInState.length > 0 && definition.mode == "listen") {
      const message = i18n("STATE.REMOVED_NEXT", { stateId });
      dispatch("adaptor/addLog", { type: "info", message }, { root: true });
      nextsInState.forEach((next) => {
        console.log("insdie delete next ", next, next[0]);
        // dispatch("deleteAction", { actionId: next[0], stateId });
        commit("DELETE_ACTION", { stateId, actionId: next[0] });
      });
    }

    // finally add new action to state
    commit("ADD_ACTION_KEY", { stateId, actionId, mode: definition.mode });
    // move next action to bottom of list
    if (nextsInState.length > 0 && definition.mode == "run")
      nextsInState.forEach((next) => commit("MOVE_ACTION_TO_END", { stateId, actionId: next[0], mode: "run" }));
    commit("SET_ACTION_DATA", {
      actionId,
      value: {
        action: definition.action,
        plugin: definition.plugin,
        mode: definition.mode,
        name: `${definition.action}_${getters.sumActionsInState(stateId, definition.action) + 1}`,
        payload: actionData || minimalObject(definition.schema.properties.payload),
      },
    });
    dispatch("syncState", stateId);
    return Promise.resolve;
  },
  deleteAction({ dispatch, getters, commit }, { actionId, stateId }) {
    dispatch("deleteContents", getters.contentIdsByAction(actionId));
    commit("DELETE_ACTION", { stateId, actionId });
    dispatch("syncState", stateId);
  },
  updateAction: ({ getters, commit, dispatch }, { actionId, value, stateId }) => {
    // FIXME: use adaptorContent flag from schema
    // TODO: only update if something changed

    // Note: to reuse existing action content ids we create a pool that gets used before new ones are created
    const existingIds = getters.contentIdsByAction(actionId);
    const contentKeys = [
      "text",
      "respond",
      "caption",
      "contains",
      "regex",
      "equals",
      "value",
      "play",
      "say",
      "flow",
      "file",
    ];
    const deepIterate = (obj) => {
      for (let key in obj) {
        if (typeof obj[key] === "object") {
          deepIterate(obj[key]);
        }
        // if content is in array of strings
        else if (contentKeys.includes(key) && Array.isArray(obj[key]) && obj[key].every((i) => typeof i !== "object")) {
          for (const index in obj[key]) {
            const contentId = existingIds.shift() || "_content_" + createId(10);
            const contentValue = obj[key][index];
            obj[key][index] = contentId;
            commit("UPDATE_CONTENT", { contentId, value: contentValue });
          }
        }
        // if content is in single strings
        else if (contentKeys.includes(key) && typeof obj[key] !== "object") {
          const contentId = existingIds.shift() || "_content_" + createId(10);
          const contentValue = obj[key];
          obj[key] = contentId;
          commit("UPDATE_CONTENT", { contentId, value: contentValue });
        }
      }
      return obj;
    };
    value = deepIterate(cloneDeep(value)); // BUG: we need to clone this, but WHY?!?
    commit("SET_ACTION_PAYLOAD", { key: actionId, value });
    if (existingIds.length >= 1) dispatch("deleteContents", existingIds); // delete remaining contentIds
    dispatch("syncState", stateId);
  },
  deleteContents({ commit }, contentIds) {
    contentIds.forEach((contentId) => commit("DELETE_CONTENT", contentId));
  },
  updateComment({ state, commit, dispatch }, { stateId, index, comment }) {
    const comments = clone(state.data.states[stateId]?.comments || []);
    if (index < 0) comments.push(comment);
    else comments[index] = comment;
    commit("UPDATE_LEVEL_STATE", { stateId, value: { comments } });
    dispatch("syncState", stateId);
  },
  deleteComment({ state, commit, dispatch }, { stateId, index }) {
    const comments = clone(state.data.states[stateId]?.comments || []);
    if (comments[index] !== undefined) {
      comments.splice(index, 1);
      commit("SET_LEVEL_STATE_KEY", { stateId, key: "comments", value: comments });
      dispatch("syncState", stateId);
    }
  },
  triggerStateNameEdit({ commit }, stateId) {
    const stateData = this.state.level.data.states[stateId];
    if (stateData.name === "START" || stateData.name === "QUIT") return;
    commit("SELECT_STATE", stateId);
    // Set a flag in state to indicate edit mode
    commit("SET_LEVEL_DATA", { key: "editingStateName", value: stateId });
  },
  triggerAddComment({ commit }, stateId) {
    commit("SELECT_STATE", stateId);
    // Set a flag in state to indicate comment mode
    commit("SET_LEVEL_DATA", { key: "addingStateComment", value: stateId });
  },
};

const mutations = {
  SELECT_LEVEL(state, level) {
    state.selected = level;
  },
  SELECT_LEVEL_ID(state, id) {
    state.id = id;
  },
  BLOCK_STATE(state, { stateId, session }) {
    state.busyStates[stateId] = session;
  },
  RELEASE_STATE(state, stateId) {
    delete state.busyStates[stateId];
  },
  SELECT_STATE(state, stateId) {
    state.selectedState = stateId;
  },
  SELECT_ACTION(state, actionId) {
    state.selectedAction = actionId;
  },
  SET_LEVEL_DATA(state, { key, value }) {
    state[key] = value;
  },
  SET_LEVEL_STATE_KEY(state, { stateId, key, value }) {
    state.data.states[stateId][key] = value;
  },
  SET_LEVEL_STATE(state, { stateId, value }) {
    state.data.states[stateId] = value;
  },
  UPDATE_LEVEL_STATE(state, { stateId, value }) {
    // TODO: is this working as expected!?
    merge(state.data.states[stateId], value);
  },
  UPDATE_STATE_POSITION(state, { stateId, position }) {
    state.data.states[stateId].view.position = position;
  },
  UPDATE_STATE_PATH(state, { stateId, path }) {
    state.data.states[stateId].path = path;
  },
  ADD_STATE(state, { stateId, stateName, position }) {
    // TODO: rework this to respect schema
    state.data.states[stateId] = {
      name: stateName || stateId,
      view: { position },
      run: [],
      listen: [],
    };
  },
  SET_ACTION_COPY(state, data) {
    state.copiedAction = data;
  },
  RESET_ACTION_COPY(state) {
    state.copiedAction = undefined;
  },
  DELETE_STATE(state, stateId) {
    delete state.data.states[stateId];
  },
  ADD_ACTION_KEY(state, { stateId, actionId, mode }) {
    state.data.states[stateId][mode].push(actionId);
  },
  DELETE_ACTION_KEY(state, { stateId, actionId, mode }) {
    const stateRef = state.data.states[stateId];
    const index = stateRef[mode].indexOf(actionId);
    stateRef[mode].splice(index, 1);
  },
  SET_ACTION_DATA(state, { actionId, value }) {
    state.data.actions[actionId] = value;
  },
  SET_ACTION_PAYLOAD(state, { key, value }) {
    state.data.actions[key].payload = value;
  },
  DELETE_ACTION(state, { stateId, actionId }) {
    const stateRef = state.data.states[stateId];
    const runIndex = stateRef["run"].indexOf(actionId);
    const listenIndex = stateRef["listen"].indexOf(actionId);
    // remove action key from state
    if (runIndex >= 0) {
      stateRef["run"].splice(runIndex, 1);
    } else if (listenIndex >= 0) {
      stateRef["listen"].splice(listenIndex, 1);
    }
    // remove action data
    delete state.data.actions[actionId];
  },
  MOVE_ACTION_TO_END(state, { stateId, actionId, mode }) {
    const actions = state.data.states[stateId][mode];
    actions.push(actions.splice(actions.indexOf(actionId), 1)[0]);
  },
  SHIFT_ACTION(state, { stateId, index, mode, direction }) {
    const newArray = cloneDeep(state.data.states[stateId][mode]);
    const newIndex = index + direction;
    if (newIndex >= newArray.length) {
      var k = newIndex - newArray.length + 1;
      while (k--) {
        newArray.push(undefined);
      }
    }
    newArray.splice(newIndex, 0, newArray.splice(index, 1)[0]);
    state.data.states[stateId][mode] = newArray;
  },
  SET_CONTENT(state, { contentId, value }) {
    state.data.contents[contentId] = value;
  },
  UPDATE_CONTENT(state, { contentId, value }) {
    // FIXME: add language support
    state.data.contents[contentId] = { de: value };
  },
  DELETE_CONTENT(state, contentId) {
    delete state.data.contents[contentId];
  },
  CLEAR_LEVEL(state) {
    state.selected = "";
    state.id = "";
    state.data = {};
    state.schema = {};
    state.variables = {};
    state.selectedState = "";
    state.selectedAction = "";
    // state.copiedAction = undefined;
    state.busyStates = {};
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
