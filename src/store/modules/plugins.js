// FIXME: remove this and have no unused vars
/* eslint-disable no-unused-vars */

import { i18n, createId } from "@utils";

const state = {
  plugins: [], // list of all installed plugins in game
  pluginsNames: [], // list of all names of installed plugins in game
  availablePlugins: [], // list of all available plugins in adaptor
};

const getters = {
  availablePlugins(state) {
    return state.availablePlugins.filter((a) => !state.pluginsNames.includes(a.name));
  },
};

const actions = {
  clear({ commit }) {
    commit("CLEAR_PLUGINS_DATA");
  },
  getAvailable({ commit }) {
    this.$api
      .getPlugins()
      .then((r) => commit("SET_AVAILABLE_PLUGINS_DATA", r.data))
      .catch(console.error);
  },
  getInstalled({ rootState, commit }, game) {
    if (!game) game = rootState.game.selected;
    this.$api
      .findPlugins({ game })
      .then((res) => {
        commit("SET_INSTALLED_PLUGINS_DATA", res.data);
      })
      .catch(console.error);
  },
  updateInstalled({ commit }, plugins) {
    commit("SET_INSTALLED_PLUGINS_DATA", plugins);
  },
  install({ rootState, dispatch }, { game, plugin }) {
    if (!game) game = rootState.game.selected;
    this.$api
      .addPlugin({ game }, { name: plugin })
      .then(() => {
        dispatch("getInstalled"); // ToDo: this is not necessary if game socket plugins update is processed
      })
      .catch(console.error);
  },
  remove({ rootState, dispatch }, { game, plugin }) {
    if (!game) game = rootState.game.selected;
    this.$api
      .removePlugin({ game, plugin })
      .then(() => {
        dispatch("getInstalled"); // ToDo: this is not necessary if game socket plugins update is processed
      })
      .catch(console.error);
  },
  updateSettings({ rootState, dispatch }, { game, plugin, data }) {
    if (!game) game = rootState.game.selected;
    this.$api
      .updatePluginSettings({ game, plugin }, data)
      .then(() => dispatch("load", { game, plugin }))
      .catch(console.error);
  },
  load({ rootState, commit, dispatch }, { game, plugin }) {
    if (!game) game = rootState.game.selected;
    this.$api
      .loadPlugin({ game, plugin })
      .then((res) => commit("SET_PLUGIN_DATA", { plugin: res.data.name, value: res.data }))
      // .then(() => {
      //   const message = i18n("PLUGINS.UPDATE", { name: plugin.toUpperCase() });
      //   dispatch("adaptor/addLog", { type: "info", message }, { root: true });
      // })
      .catch(console.error);
  },
  // connect({ rootState, dispatch }, { game, plugin }) {
  //   if (!game) game = rootState.game.selected;
  //   this.$api
  //     .connectPlugin({ game, plugin }) // TODO: add settings if you like
  //     .then((res) => {
  //       console.log("connectPlugin result:", res);
  //       dispatch("loadPlugin", { plugin });
  //     })
  //     .catch(console.error);
  // },
  // disconnect({ state, dispatch }, { plugin }) {
  //   const game = state["selected"]["game"];
  //   this.$api
  //     .disconnectPlugin({ game, plugin })
  //     .then((res) => {
  //       console.log("disconnectPlugin result:", res);
  //       dispatch("loadPlugin", { plugin });
  //     })
  //     .catch(console.error);
  // },
  getItems({ rootState, commit }, { game, plugin, collection }) {
    if (!game) game = rootState.game.selected;
    this.$api
      .findPluginItems({ game, plugin, plugin_collection: collection })
      .then((res) => commit("SET_PLUGIN_ITEMS", { plugin, collection, value: res.data }))
      .catch(console.error);
  },
  addItem({ rootState, dispatch }, { game, plugin, collection, data }) {
    if (!game) game = rootState.game.selected;
    if (data.name === undefined || data.name == "") data.name = `${collection}_${createId(5)}`;
    return this.$api
      .createPluginItem({ game, plugin, plugin_collection: collection }, data)
      .then(() => dispatch("getItems", { game, plugin, collection })) // Note: we don't need this request when the socket is implemented
      .catch(console.error);
  },
  editItem({ rootState, dispatch }, { game, plugin, collection, item, data }) {
    if (!game) game = rootState.game.selected;
    return this.$api
      .editPluginItem({ game, plugin, plugin_collection: collection, item }, data)
      .then(() => dispatch("loadItem", { game, plugin, collection, item }))
      .catch(console.error);
  },
  removeItem({ rootState, dispatch }, { game, plugin, collection, item }) {
    if (!game) game = rootState.game.selected;
    this.$api
      .deletePluginItem({ game, plugin, plugin_collection: collection, item })
      .then(() => dispatch("getItems", { game, plugin, collection })) // Note: we don't need this request when the socket is implemented
      .catch(console.error);
  },
  updateItems({ rootState, commit }, items) {
    // if we are not in settings view we don't care about items updates from socket
    if (rootState.adaptor.view !== "Settings") return;
    items.forEach((item) => {
      const { plugin, collection, type, ...value } = item;
      if (type === "plugin") commit("SET_PLUGIN_ITEM", { plugin, collection, value });
    });
  },
  removeItems({ rootState, commit }, itemIds) {
    // if we are not in settings view we don't care about items updates from socket
    if (rootState.adaptor.view !== "Settings") return;
    itemIds.forEach((id) => {
      console.log(id);
      const plugin = ""; // FIXME: socket sends only id of item we need to find it in plugins
      const collection = ""; // FIXME: socket sends only id of item we need to find it in plugins
      // commit("REMOVE_PLUGIN_ITEM", { plugin, collection, id });
    });
  },
  connectItem({ rootState, dispatch }, { game, plugin, collection, item }) {
    if (!game) game = rootState.game.selected;
    this.$api.connectPluginItem({ game, plugin, plugin_collection: collection, item }).catch(console.error);
  },
  disconnectItem({ rootState, dispatch }, { game, plugin, collection, item }) {
    if (!game) game = rootState.game.selected;
    this.$api
      .disconnectPluginItem({
        game,
        plugin,
        plugin_collection: collection,
        item,
      })
      .then(() => {
        dispatch("load", { plugin });
      })
      .catch(console.error);
  },
  loadItem({ rootState, commit }, { game, plugin, collection, item }) {
    if (!game) game = rootState.game.selected;
    this.$api
      .loadPluginItem({
        game,
        plugin,
        plugin_collection: collection,
        item,
      })
      .then((res) => {
        commit("SET_PLUGIN_DATA", { plugin: res.data.name, value: res.data });
      })
      .catch(console.error);
  },
};

const mutations = {
  CLEAR_PLUGINS_DATA(state) {
    state.availablePlugins = [];
    state.plugins = [];
    state.pluginsNames = [];
  },
  SET_AVAILABLE_PLUGINS_DATA(state, value) {
    state.availablePlugins = value;
  },
  SET_INSTALLED_PLUGINS_DATA(state, value) {
    state.plugins = value;
    state.pluginsNames = value.map((p) => p.name);
  },
  SET_PLUGIN_DATA(state, { plugin, value }) {
    const index = state.plugins.findIndex((p) => p.name == plugin);
    if (index < 0) {
      const message = i18n("PLUGINS.NOTFOUND", { name: plugin });
      console.error(message);
      return;
    }
    state.plugins[index] = value;
  },
  SET_PLUGIN_ITEMS(state, { plugin, collection, value }) {
    const index = state.plugins.findIndex((p) => p.name == plugin);
    if (index < 0) {
      const message = i18n("PLUGINS.NOTFOUND", { name: plugin });
      console.error(message);
      return;
    }
    state.plugins[index]["items"][collection]["items"] = value;
  },
  SET_PLUGIN_ITEM(state, { plugin, collection, value }) {
    const pluginIndex = state.plugins.findIndex((p) => p.name == plugin);
    if (pluginIndex < 0) {
      const message = i18n("PLUGINS.NOTFOUND", { name: plugin });
      console.error(message);
      return;
    }
    const items = state.plugins[pluginIndex]?.["items"]?.[collection]?.["items"];
    if (!items) {
      console.warn("got plugin item update but data not yet loaded", plugin);
      return;
    }
    const itemIndex = items.findIndex((i) => i._id == value._id);
    if (itemIndex < 0) items.push(value);
    else items[itemIndex] = value;
  },
  REMOVE_PLUGIN_ITEM(state, { plugin, collection, id }) {
    const pluginIndex = state.plugins.findIndex((p) => p.name == plugin);
    if (pluginIndex < 0) {
      const message = i18n("PLUGINS.NOTFOUND", { name: plugin });
      console.error(message);
      return;
    }
    const items = state.plugins?.[pluginIndex]?.["items"]?.[collection]?.["items"];
    if (!items) {
      console.warn("got plugin item update but data not yet loaded", plugin);
      return;
    }
    const itemIndex = items.findIndex((i) => i._id == id);
    items.splice(itemIndex, itemIndex >= 0 ? 1 : 0);
  },
  UPDATE_PLUGIN_DATA(state, { name, data }) {
    console.error("UPDATE_PLUGIN_DATA NOT IMPLEMENTED", name, data);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
