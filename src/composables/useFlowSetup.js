import { ref, computed } from "vue";
import { useVueFlow } from "@vue-flow/core";

export function useFlowSetup({ store, onNodeSelect, onNodePositionChange }) {
  const options = {
    deleteKeyCode: false,
    selectionKeyCode: "Shift",
    zoomActivationKeyCode: "Meta",
    zoomOnScroll: false,
    panOnScroll: true,
    panOnScrollMode: "Free",
    fitViewOnInit: false,
    defaultViewport: { zoom: 0.75 },
    maxZoom: 1.5,
    minZoom: 0.25,
    snapToGrid: true,
    snapGrid: [1, 1],
  };

  const flow = useVueFlow(options);
  const isNewLevelView = ref(true);
  let multiSelection = false;

  // Computed properties
  const stateNodes = computed(() => {
    if (!store.state.level.data.states) return [];
    return Object.entries(store.state.level.data.states).map(([sId, v]) => ({
      id: v.name,
      type: "adaptorState",
      data: { id: sId },
      ...v.view,
    }));
  });

  // Context menu state
  const showContextMenu = ref(false);
  const contextMenuPosition = ref({ x: 0, y: 0 });
  const contextMenuTarget = ref(null);
  const lastViewport = ref({ x: 0, y: 0 });

  function handleContextMenu(event, target = null) {
    event.preventDefault();
    showContextMenu.value = true;
    contextMenuPosition.value = { x: event.clientX, y: event.clientY };

    if (event.contextMenuType === "action") {
      contextMenuTarget.value = {
        stateId: target, // state id
        actionId: event.actionId,
        type: "action",
      };
    } else {
      contextMenuTarget.value = target
        ? {
            stateId: target === "pane" ? null : target,
            actionId: null,
            type: target === "pane" ? "pane" : "state",
          }
        : null;
    }

    lastViewport.value = { ...flow.viewport.value };
  }

  function closeContextMenu() {
    showContextMenu.value = false;
  }

  // Flow event handlers
  flow.onPaneReady(() => initView());
  flow.onSelectionStart(() => (multiSelection = true));
  flow.onSelectionEnd(() => (multiSelection = false));
  flow.onPaneClick(() => store.dispatch("level/unselectAll"));
  flow.onPaneContextMenu((event) => handleContextMenu(event, "pane"));
  flow.onNodeContextMenu((event, node) => {
    // If it's an action context menu, the event will have contextMenuType set
    // If not, it's a state context menu
    handleContextMenu(event, node.data.id);
  });

  flow.onNodesChange((changes) => {
    for (const change of changes) {
      switch (change.type) {
        case "select":
          if (multiSelection) return;
          if (change.selected) {
            const stateId = flow.findNode(change.id).data.id;
            onNodeSelect(stateId);
          }
          break;
        case "position":
          if (!change.dragging) {
            const node = flow.findNode(change.id);
            const stateId = node.data.id;
            const position = node.position;
            if (position.x == change.from.x && position.y == change.from.y) return;
            onNodePositionChange({ stateId, position });
          }
          break;
        case "dimensions":
          if (change.id == "START" && isNewLevelView.value) initView();
          break;
      }
    }
  });

  flow.onViewportChange(({ x, y }) => {
    if (showContextMenu.value) {
      const dx = x - lastViewport.value.x;
      const dy = y - lastViewport.value.y;
      contextMenuPosition.value = {
        x: contextMenuPosition.value.x + dx,
        y: contextMenuPosition.value.y + dy,
      };
      lastViewport.value = { x, y };
    }
  });

  function initView() {
    flow.fitView({
      nodes: ["START"],
      maxZoom: 0.5,
      offset: { x: -200, y: -200 },
      duration: 500,
    });
    isNewLevelView.value = false;
  }

  return {
    flow,
    stateNodes,
    showContextMenu,
    contextMenuPosition,
    contextMenuTarget,
    handleContextMenu,
    closeContextMenu,
    initView,
    isNewLevelView,
  };
}
